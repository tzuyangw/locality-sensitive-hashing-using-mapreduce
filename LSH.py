from pyspark import SparkConf, SparkContext
import os
from random import randrange
from itertools import combinations

K_SHINGLES = 3
NUM_HASHES = 100
ROW = 2
BUCKET_NUM = 25

bucket = []
document = []
document_num = 0

dictionary = dict()
shingle_num = 0
shingle_matrix = []

### Data preprocess ###
def data_preprocess():
    global document_num, document
    for id in sorted(os.listdir("athletics/")):
        if id.endswith(".txt"):
            filepath = "athletics/" + id
            file = open(filepath, 'r')

            # Replace all the characters that will effect the split of the words
            data = file.read().replace("\n", " ").replace(","," ").replace(".", " ").replace('"', " ").split()

            # Tranform data into parallelize format
            data = sc.parallelize(data)
            document.append(data)
            document_num += 1

### Shingle ###
def shingling():
    global dictionary, shingle_matrix, shingle_num
    for id in range(0, document_num):
        temp = []
        words = document[id].collect()
        for i in range(len(words) - K_SHINGLES + 1):
            
            # Makeing the 3-shingle elements
            shingle = words[i:i + K_SHINGLES]
            
            # Eliminate the '[' and ']'
            shingle = ' '.join(shingle)
            temp.append(shingle)

            # Judge wether the shingle have appeared before
            if shingle not in dictionary :
                dictionary[shingle] = shingle_num
                shingle_num += 1

        # Add the single list to the shingle matrix
        shingle_matrix.append(temp)

### Min Hash ###
def hash_function(a, b, sig):
    hashes = [((a * x) + b) % shingle_num for x in sig]
    return min(hashes)

def min_hash(sig):
    hashes = [hash_function(a, b, sig) for a, b in zip(a_hash, b_hash)]
    return hashes

### LSH ###
def LSH():
    global bucket

    # Claer all of the buckets
    for i in range(0,BUCKET_NUM):
        bucket.append([])

    # Choose a random band(0-50)
    random_band = randrange(0, 50, 2)
    for id in range(0, document_num):
        # Hash the signature into bucket
        hash2bucket = (signatures[id][random_band] * signatures[id][random_band + 1]) % BUCKET_NUM
        bucket[hash2bucket].append(id)

    result=[]
    # Check each buckets and Calculate the jaccard similarity between each documents in the same bucket
    for index in range(0,BUCKET_NUM):
        if int(len(bucket[index]))>=2:
            for i,j in list(combinations(bucket[index], 2)):
                a = set()
                b = set()
                for k in range(len(shingle_matrix[i])):
                    a.add(shingle_matrix[i][k])
                for k in range(len(shingle_matrix[j])):
                    b.add(shingle_matrix[j][k])
                score=(len(a&b)/len(a|b))*100
                result.append(list([i+1,j+1,score]))
    
    # Sort the result of the similarities and print out the top 10 pairs
    result=sc.parallelize(result).sortBy(lambda x: x[2], ascending= False).collect()
    for i in range(0,10):
        print("(" + str(result[i][0]) + "," + str(result[i][1]) + "):",round(result[i][2],2),"%")
    
            
conf = SparkConf().setMaster("local").setAppName("LSH")
sc = SparkContext(conf = conf)

print("......Data Preprocess......")
data_preprocess()
print("Done")

print("......Shingling......")
shingling()
print("Done")

print("......Min Hashing......")
a_hash = [randrange(0,shingle_num) for _ in range(0, NUM_HASHES)]
b_hash = [randrange(0,shingle_num) for _ in range(0, NUM_HASHES)]

signatures=[]
for id in range(0,document_num):
    doc = sc.parallelize(shingle_matrix[id])
    
    # First use map function to earn the corresponding indices
    signature = doc.map(lambda x: dictionary.get(x))

    # Then hash the document into signature
    hashed_result = min_hash(signature.collect())
    signatures.append(hashed_result)
print("Done")

print("......LSH......")
LSH()
print("Done")